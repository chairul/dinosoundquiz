﻿using UnityEngine;

public class Dinosaurus {

	public string Name{ get; set; }

	public Dinosaurus(string dinoName){
		this.Name = dinoName;
	}
}