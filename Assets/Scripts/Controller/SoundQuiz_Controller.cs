﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SimpleJSON;

public class SoundQuiz_Controller : MonoBehaviour {

	public static SoundQuiz_Controller instance;
	[SerializeField] Text textSoal, textFalse, textTrue;
	[SerializeField] int questionCount;
	[SerializeField] List<GameObject> choices;
	[SerializeField] GameObject panelGameFinish;

	Dictionary<string,bool> askedDino = new Dictionary<string,bool> ();
	Dictionary<string,bool> shownDino = new Dictionary<string,bool> ();
	List<string> listDinoName;
	GameObject[] instantiated;

	JSONNode JSONLoaded;
	[HideInInspector] public string currentDino;
	int curQuestionCount = 0, countTrue = 0, countFalse = 0;

	void Awake() {
		instance = this;
	}

	void Start () {
		LoadJSON ("Data/soundquiz");
	}

	void LoadJSON(string url){
		string json = Resources.Load<TextAsset>(url).text;
		OnLoadJSON (json);
	}

	void OnLoadJSON(string json){
		JSONLoaded = JSON.Parse(json);

		listDinoName = new List<string> ();
		for (int i = 0; i < JSONLoaded ["SoundQuiz"] ["dinos"].Count; i++) {
			listDinoName.Add (JSONLoaded ["SoundQuiz"] ["dinos"] [i] ["name"]);
		}

		RandomDino ();
	}

	public void RandomDino(){
		if (curQuestionCount < questionCount) {
			if (shownDino.Count > 0) {
				ClearPreviousDino ();
			}

			SetLayoutContent (Random.Range (0, JSONLoaded ["SoundQuiz"] ["dinos"].Count));
		} else {
			GameFinish ();
		}
	}

	void SetLayoutContent(int indexDinoAnswer){
		// if dino has already asked/called, call other dino
		if (askedDino.Count > 0 && IsAlreadyAsked (JSONLoaded ["SoundQuiz"] ["dinos"] [indexDinoAnswer] ["name"])) {
			RandomDino ();
		} else {
			instantiated = new GameObject[3];

			JSONNode curNode = JSONLoaded ["SoundQuiz"] ["dinos"] [indexDinoAnswer];
			string dinoName = curNode ["name"];

			currentDino = dinoName;
			textSoal.text = dinoName;

			shownDino.Add (currentDino, true);
			askedDino.Add (currentDino, true);
			SetOtherDino ();

			curQuestionCount++;
		}
	}

	bool IsAlreadyAsked(string key){
		return askedDino.ContainsKey (key);
	}

	void ClearPreviousDino(){
		foreach (GameObject go in instantiated) {
			Destroy (go);
		}
		shownDino.Clear ();
	}

	void SetOtherDino(){
		// Randomize Dino List
		for (int i = 0; i < listDinoName.Count; i++) {
			string temp = listDinoName [i];
			int randomIndex = Random.Range (i, listDinoName.Count);
			listDinoName [i] = listDinoName [randomIndex];
			listDinoName [randomIndex] = temp;
		}

		int otherDino = 2;

		for (int i = 0; i < listDinoName.Count; i++) {
			if (!shownDino.ContainsKey(listDinoName [i]) && otherDino > 0) {
				shownDino.Add (listDinoName [i], true);
				otherDino--;

				if (otherDino == 0) {
					UpdateLayoutContent ();
				}
			}
		}
	}

	void UpdateLayoutContent(){
		shownDino.Shuffle();
		ShuffleAnswerPosition ();

		int loop = 0;
		foreach (KeyValuePair<string,bool> pair in shownDino) {
			PutDinoAsChoice (pair.Key, loop);
			loop++;
		}

		PlaySound ();
	}

	void PutDinoAsChoice(string dinoName, int pos){
		int indexDino = 0;

		// Finding correct dino index from json by given name
		for (int i = 0; i < JSONLoaded ["SoundQuiz"] ["dinos"].Count; i++) {
			if (dinoName.Equals (JSONLoaded ["SoundQuiz"] ["dinos"] [i] ["name"]))
				indexDino = i;
		}

		JSONNode curNode = JSONLoaded ["SoundQuiz"] ["dinos"] [indexDino];
		Vector2 dinoPos = new Vector2 (curNode ["position"] ["x"].AsFloat, curNode ["position"] ["y"].AsFloat);
		Vector2 dinoScale = new Vector2 (curNode ["scale"] ["x"].AsFloat, curNode ["scale"] ["y"].AsFloat);

		instantiated[pos] = Instantiate (Resources.Load<GameObject> ("Prefabs/Dinosaur/Dino " + dinoName), choices [pos].transform);
		instantiated[pos].transform.localPosition = dinoPos;
		instantiated[pos].transform.localScale = dinoScale;
		instantiated[pos].AddComponent <SoundQuiz_Item> ().Create (dinoName);
	}

	void ShuffleAnswerPosition(){
		for (int i = 0; i < choices.Count; i++) {
			GameObject temp = choices [i];
			int randomIndex = Random.Range (i, choices.Count);
			choices [i] = choices [randomIndex];
			choices [randomIndex] = temp;
		}
	}

	public void PlaySound(){
		AudioClip clip = Resources.Load<AudioClip> ("VO/Dino Name/vo_" + currentDino);
		SoundManager.instance.PlaySingle (clip);
	}

	void UpdateScoreUI(){
		textFalse.text = countFalse.ToString ();
		textTrue.text = countTrue.ToString ();
	}

	public void NotifyAnswer(bool answer, SoundQuiz_Item dino){
		if (answer) {
			countTrue++;
			UpdateScoreUI ();

			dino.gameObject.SetActive (false);
			DisableDinoClick ();
			Invoke ("RandomDino", 1f);
		} else {
			countFalse++;
			UpdateScoreUI ();

			dino.isActive = false;
			DisableDinoClick ();
			Invoke ("RandomDino", 1f);
		}
	}

	void DisableDinoClick(){
		foreach (GameObject go in instantiated) {
			go.GetComponent<SoundQuiz_Item> ().isActive = false;
		}
	}

	void GameFinish(){
		panelGameFinish.SetActive (true);
	}
}
