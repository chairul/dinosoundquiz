﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour{
	AudioSource source;
	public static SoundManager instance;

	void Awake(){
		instance = this;

		source = GetComponent<AudioSource> ();
	}

	public void PlaySingle(AudioClip clip){
		if (!source.isPlaying) {
			source.clip = clip;
			source.Play ();
		}
	}
}