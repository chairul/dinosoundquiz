﻿using UnityEngine;

public class SoundQuiz_Item : MonoBehaviour {
	
	public Dinosaurus dino;
	public bool isActive = true;

	public void Create(string name) {
		dino = new Dinosaurus (name);
	}

	void OnMouseUp(){
		if (isActive) {
			if (dino.Name.Equals (SoundQuiz_Controller.instance.currentDino)) {
				SoundQuiz_Controller.instance.NotifyAnswer (true, this);
			} else {
				SoundQuiz_Controller.instance.NotifyAnswer (false, this);
			}
		}
	}
}