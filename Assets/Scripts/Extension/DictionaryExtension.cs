﻿using System.Collections.Generic;
using System.Linq;
using Random = System.Random;

public static class DictionaryExtension
{
	public static Dictionary<TKey, TValue> Shuffle<TKey, TValue>(this Dictionary<TKey, TValue> source){
		Random r = new Random();
		return source.OrderBy(x => r.Next())
			.ToDictionary(item => item.Key, item => item.Value);
	}
}